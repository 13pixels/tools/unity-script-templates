# What's inside
A set of neatly ordered script templates for the Unity Editor.
![Create Menu](https://i.imgur.com/g7KGq7W.png)

The templates are updated to
* include more templates, like `ScriptableObject` and `EditorWindow`.
* come in a nice hierarchy.
* have 4 spaces instead of tabs (you can have tabs in the `tabs` branch, though).
* have consistent line endings (comes free if your git client does a proper os-specific checkout).
* change a few smaller things from vanilla templates.

The MonoBehaviour template comes with `Awake` and `Update`.

Of course, feel free to change anything for yourself.

# How to use
* Open your Editor Folder.
  * With Unity Hub: `[..]/Programs/Unity/Hub/Editor/[version]/Editor`
  * Without Unity Hub: `[..]/Programs/Unity/Editor`
* Open your Script Templates folder.
  * `Data/Resources/ScriptTemplates`
* Optional: Backup the contained files.
* Replace the contained files with this repository.