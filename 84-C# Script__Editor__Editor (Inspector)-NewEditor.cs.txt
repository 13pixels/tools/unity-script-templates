﻿
    #ROOTNAMESPACEBEGIN#
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CLASS))]
public class #SCRIPTNAME# : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        #NOTRIM#

        serializedObject.ApplyModifiedProperties();
    }
}
#ROOTNAMESPACEEND#
